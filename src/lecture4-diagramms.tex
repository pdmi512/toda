%% 02.04.20
%%
\section*{Диаграммная техника}
В этой главе мы получили представление Гаусса-Гивенталя для $N$-частичной волновой функции
\begin{equation}
	\Psi (\pmb{x} | \pmb \lambda ) = 
	\int \dd{}^{N - 1} \pmb y \, \Lambda( \pmb x | \pmb y, \lambda_N) \Psi (\pmb y | \pmb \lambda'),
\end{equation}
где ядро оператора задаётся соотношением
\begin{equation}
	\Lambda_N (\pmb x | \pmb y ', u) =
	\exp\left[
		i u \sum_{n = 1}^{N} x_n
		-i u \sum_{n = 1}^{N - 1} y_n
		-\sum_{n=1}^{N-1}
		\left(
			e^{ y_{n} - x_{n + 1}} + e^{x_{n} - y_{n}}
		\right)
	\right].
\end{equation}
Здесь $\pmb x = (x_1, \dots, x_N)$, $\pmb \lambda = (\lambda_1, \dots, \lambda_N)$ и $\pmb y ' = (y_1, \dots, y_{N - 1})$.

Однако нам ещё осталось показать, что такая волновая функция инвариантна относительно перестановок квантовых чисел $\left\{\lambda_i\right\}_1^N$. Оказывается, что это довольно просто сделать при помощи диаграммной техники. Заведем правила Фейнмана в следующем виде
\begin{equation}
\begin{aligned}
	\begin{tikzpicture}[
		baseline = {(y.base)}]
	\def\r{2}
	\draw [thick, marrow] (0, 0) coordinate(x) -- ++(\r, 0)
		node[midway, above] {$v$} coordinate(y)
		node[midway, below] {\phantom{$v$}};
	\draw[fill]
		(x) node[left] {$x$}
		(y) node[right] {$y$};
	\end{tikzpicture}
	&= \exp \left( i v (x - y) - e^{x - y} \right)\\
	\begin{tikzpicture}[
		baseline = {(y.base)}]
	\def\r{2}
	\draw [thick, tail] (0, 0) coordinate(x) -- ++(0.75 * \r, 0)
		node[pos = 0.66, above] {$v$}
		++ (0.25 * \r, 0) coordinate(y);
	\draw[fill]
		(x) node[left] {$x$}
		(y) node[right] {$\phantom{y}$};
	\end{tikzpicture}
	&= \exp \left( i v x \right)
\end{aligned}
\end{equation}

Чтобы проиллюстрировать, как пользоваться правилами, построим ядро оператора $\Lambda_N$ для $N = 2$
\begin{equation}
	\Lambda_2 (x_1, x_2 | y_1, u) = \exp \left[
		i u (x_1 + x_2 ) - i u y_1 -
		\left( e^{y_1 - x_2} + e^{x_1 - y_1} \right) 
	\right].
\end{equation}
Перегруппируем слагаемые в экспоненте и нарисуем диаграмму
\begin{equation}
	\exp \left[
		i u (x_1 - y_1) - e^{x_1 - y_1}
		\textcolor{blue}{- e^{y_1 - x_2}}
		\textcolor{green}{+ i u x_2}
	\right] = 
	\begin{tikzpicture}[baseline = {(y1.base)}]\scriptsize
	\def\r{1.25}
	\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
		node[midway, above] {$u$} coordinate(y1);
	\draw  [thick, marrow, blue] (y1) -- ++(180 + 30 : \r)
		node[midway, above] {$0$} coordinate(x2);
	\draw [thick, tail, green] (x2) -- ++(-30 : 0.75 * \r)
			node[black, below right] {$u$};
	\draw
		(x1) node[left] {$x_1$}
		(x2) node[left] {$x_2$}
		(y1) node[right] {$y_1$};
	\end{tikzpicture}
\end{equation}
Теперь построим двухчастичную волновую функцию. Для этого нужно свернуть оператор $\Lambda_2$ с одночастичной волновой функцией
\begin{equation}
	\Psi_2 (x_1, x_2 | \lambda_1, \lambda_2) = 
	\int \dd{y} \Lambda( x_1, x_2 | y, \lambda) \ e^{i \lambda y} = 
	\begin{tikzpicture}[baseline = {(y1.base)}]\scriptsize
	\def\r{1.25}
	\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
		node[midway, above] {$\lambda_2$} coordinate(y1);
	\draw  [thick, marrow] (y1) -- ++(180 + 30 : \r)
		node[midway, above] {$0$} coordinate(x2);
	\draw [thick, tail] (x2) -- ++(-30 : 0.75 * \r)
		node[below right] {$\lambda_2$};
	\draw [thick, tail] (y1) -- ++(-30 : 0.75 * \r)
		node[below right] {$\lambda_1$};
	\draw [fill] (y1) circle(2pt);
	\draw
		(x1) node[left] {$x_1$}
		(x2) node[left] {$x_2$}
		(y1) node[above right] {$y_1$};
	\end{tikzpicture}
\end{equation}
По выделенной вершине проводится интегрирование. Посмотрев внимательно на эти примеры, можно построить ядро оператора $\Lambda_N$ и волновую функцию в случае $N$ частиц.

\begin{figure}[ht]
	\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\begin{tikzpicture}[baseline = {(y1.base)}]\scriptsize
			\def\r{1.25}
			\def\n{4}
			\coordinate (x1) at (0, 0);
			\foreach \n in {1, 2, 3}{
				\draw [thick, marrow] (x\n) -- ++(-30 : \r)
					node[midway, above = 1pt] {$v$} coordinate(y\n);
				\pgfmathsetmacro{\m}{int(round(\n + 1))};
				\draw [thick, marrow] (y\n) -- ++(180 + 30 : \r)
					node[midway, above = 1pt] {$0$} coordinate(x\m);
				\draw
					(x\n) node[left] {$x_\n$}
					(y\n) node[right] {$y_\n$};
			}
			\draw (x4) node[left] {$x_4$};
			\draw [thick, tail] (x4) -- ++(-30 : 0.75 * \r)
				node[below right] {$v$};
		\end{tikzpicture}
		\caption{}
		\label{fig:Lambda_N}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\begin{tikzpicture}[baseline = {(y1.base)}]\scriptsize
			\def\r{1.25}
			\def\n{4}
			\coordinate (x1) at (0, 0);
			\foreach \n in {1, 2, 3}{
				\draw [thick, marrow] (x\n) -- ++(-30 : \r)
				node[midway, above = 1pt] {$\lambda_1$} coordinate(y\n);
				\pgfmathsetmacro{\m}{int(round(\n + 1))};
				\draw [thick, marrow] (y\n) -- ++(180 + 30 : \r)
				node[midway, above = 1pt] {$0$} coordinate(x\m);
				\draw[fill]
					(y\n) circle(2pt);
				\draw
					(x\n) node[left] {$x_\n$};
			}
			\draw[thick, tail] (x4) node[left] {$x_4$} -- ++(-30 : 0.75 * \r)
				node[below right] {$\lambda_1$};
			\foreach \n in {1, 2}{
				\draw [thick, marrow] (y\n) -- ++(-30 : \r)
					node[midway, above = 1pt] {$\lambda_2$} coordinate(z\n);
				\pgfmathsetmacro{\m}{int(round(\n + 1))};
				\draw [thick, marrow] (z\n) -- ++(180 + 30 : \r)
					node[midway, above = 1pt] {$0$} coordinate(y\m);
				\draw[fill] (z\n) circle(2pt);
			}
			\draw [thick, tail] (y3) -- ++(-30 : 0.75 * \r)
				node[below right = 1pt] {$\lambda_2$};
			\draw [thick, marrow] (z1) -- ++(-30 : \r)
				node[midway, above = 1pt] {$\lambda_3$} coordinate(w1);
			\draw [thick, marrow] (w1) -- ++(180 + 30 : \r)
				node[midway, above = 1pt] {$0$} coordinate(z2);
			\draw[fill] (w1) circle(2pt);
			\draw [thick, tail] (z2) -- ++(-30 : 0.75 * \r)
				node[below right] {$\lambda_3$};
				\draw [thick, tail] (w1) -- ++(-30 : 0.75 * \r)
				node[below right] {$\lambda_4$};
		\end{tikzpicture}
		\caption{}
		\label{fig:Psi_N}
	\end{subfigure}
	\caption{Диаграммы в случае $N = 4$ для: (a) оператора $\Lambda_n$, (b) волновой функции $\Psi_N$.}
	\label{fig:Diagramms}
\end{figure}

С диаграммами мы разобрались, теперь покажем инвариантность построенной функции относительно перестановок параметров $\lambda$. Если докажем для транспозиции двух параметров, то автоматически получим доказательство для произвольной перестановки.

Для доказательства нам понадобятся несколько диаграммных тождеств. Основным тождеством является сплетающее соотношение, изображённое на рисунке~\ref{fig:Intertwining-relation}. Это тождество позволяет нам переставлять параметры на рёбрах, протаскивая вспомогательное ребро $\textcolor{blue}{S_{u - v}}$ через диаграмму.
\begin{figure}[ht]
	\centering
	\begin{tikzpicture}\small
		\def\r{1.5}
		\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
			node[midway, above = 1pt] {$v$} coordinate(w)
				++(30 : \r) coordinate(y0)
				++ (-30 : \r) coordinate(O)	%%Для знака равно
				++ (30 : \r) coordinate(X1);	%%Для правой части
		\draw [thick, marrow] (w) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$} coordinate(x2);
		\draw [thick, marrow] (y0) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$};
		\draw [thick, marrow] (w) -- ++ (-30 : \r)
			node[midway, above = 1pt] {$u$} coordinate(y1);
		\draw[fill] (w) circle(2pt);
		\draw[thick, dashed, blue, marrow] (y0) to[out = 165, in = 15]
			node[midway, above = 1pt] {$S_{u - v}$} (x1);
%		Вторая часть
		\draw [thick, marrow] (X1) -- ++(-30 : \r)
			node[midway, above = 1pt, blue] {$u$} coordinate(W) ++(30 : \r) coordinate(Y0);
		\draw [thick, marrow] (W) -- ++(180 + 30 : \r)
			node[midway, above = 1pt, blue] {$0$} coordinate(X2);
		\draw [thick, marrow] (Y0) -- ++(180 + 30 : \r)
			node[midway, above = 1pt, blue] {$0$};
		\draw [thick, marrow] (W) -- ++ (-30 : \r)
			node[midway, above = 1pt, blue] {$v$} coordinate(Y1);
		\draw[fill] (W) circle(2pt);
		\draw[thick, dashed, blue, marrow] (Y1) to[out = 195, in = -15]
			node[midway, above = 1pt] {$S_{u - v}$} (X2);
		\node at (O) {\Large$=$};
		\foreach \n in {0, 1}{
			\pgfmathsetmacro{\m}{int(round(\n + 1))};
			\draw
				(x\m) node[left] {$x_\m$}
				(X\m) node[left] {$x_\m$}
				(y\n) node[right] {$y_\n$}
				(Y\n) node[right] {$y_\n$};
		}
	\end{tikzpicture}
	\caption{Сплетающее соотношение}
	\label{fig:Intertwining-relation}
\end{figure}

Сейчас предъявим функцию $S_{u - v}$, а доказательство вынесем в аппендикс~\ref{appendix:diagramms}
\begin{equation}
\label{eq:S-intertwining}
	S_{u - v} (y, x) = \left( 1 + e^{y - x} \right)^{- i (u - v)}.
\end{equation}
%Допустим, что сплетающее соотношение выполнено для некоторой функции $S_{u - v}$. Тогда попробуем вставить такую функцию в диаграмму.

Заметим, что функция \ref{eq:S-intertwining} удовлетворяет тождеству
\begin{equation}
	S_{u-v} (y, x) \cdot S_{v - u} (y, x) = 1,
\end{equation}
что на языке диаграмм позволяет нам вставлять тождественную единицу внутрь диаграммы между двумя произвольными вершинами
\begin{equation}
\label{eq:Identity-relation}
	\begin{tikzpicture}[
	baseline = {(y)}]
	\def\r{2}
	\draw[thick, dashed, blue, marrow] (\r, 0) coordinate(y)
		to[out = 195, in = -15]
		node[midway, below = 1pt] {$S_{v - u}$} (0, 0) coordinate(x);
	\draw[thick, dashed, blue, marrow] (y) to[out = 165, in = 15]
		node[midway, above = 1pt] {$S_{u - v}$} (x);
	\end{tikzpicture}
	= 1.
\end{equation}

Чтобы поменять два параметра $u \leftrightarrow v$ в диаграмме вставим тождественную единицу внутрь диаграммы и, используя сплетающие соотношение, пронесём вспомогательные рёбра до верхней и нижней границ, как показано на рисунке~\ref{fig:Intertwining-relation-usage}.
\begin{figure}[ht]
	\centering
	\begin{tikzpicture}[baseline = {(y1.base)}]\scriptsize
		\def\r{1}
		\def\n{4}
		\def\mycolor{black}
		\def\v{v}
		\def\u{u}
		\path (0, 0) coordinate(x1)
			++(-30 : \r) ++ (30 : \r) 
			coordinate(y1)	%Здесь нумерация не кононичная, но удобная для циклов
			++(-30 : \r) ++ (30 : \r)
			coordinate(xx1)
			++(-30 : \r) ++ (30 : \r)
			coordinate(yy1)
			++(-30 : \r) ++ (30 : \r)
			coordinate(xxx1)
			++(-30 : \r) ++ (30 : \r)
			coordinate(yyy1);
		\foreach \n in {1, 2, 3, 4}{
			\pgfmathsetmacro{\m}{int(round(\n + 1))};
			%% Первая
			\draw [thick, marrow] (x\n) -- ++(-30 : \r)
				node[midway, below = 1pt] {$v$} coordinate(w);
			\draw [thick, marrow] (y\n) -- ++(180 + 30 : \r);
			\draw [thick, marrow] (w) -- ++(180 + 30 : \r) coordinate(x\m);
			\draw [thick, marrow] (w) -- ++(- 30 : \r)
				node[midway, above = 1pt] {$u$} coordinate(y\m);
			\draw[fill] (w) circle(2pt);
			%% Вторая
			`\ifthenelse{\n=3 \OR \n = 2}{ 
				\def\mycolor{blue} \def\v{u} \def\u{v} }{};
			\draw [thick, marrow] (xx\n) -- ++(-30 : \r)
				node[midway, below = 1pt, \mycolor] {$\v$} coordinate(ww);
			\draw [thick, marrow] (yy\n) -- ++(180 + 30 : \r);
			\draw [thick, marrow] (ww) -- ++(180 + 30 : \r) coordinate(xx\m);
			\draw [thick, marrow] (ww) -- ++(- 30 : \r)
				node[midway, above = 1pt, \mycolor] {$\u$} coordinate(yy\m);
			\draw[fill] (ww) circle(2pt);
			%%Третья
			\draw [thick, marrow] (xxx\n) -- ++(-30 : \r)
			node[midway, below = 1pt, blue] {$u$} coordinate(www);
			\draw [thick, marrow] (yyy\n) -- ++(180 + 30 : \r);
			\draw [thick, marrow] (www) -- ++(180 + 30 : \r) coordinate(xxx\m);
			\draw [thick, marrow] (www) -- ++(- 30 : \r)
				node[midway, above = 1pt, blue] {$v$} coordinate(yyy\m);
			\draw[fill] (www) circle(2pt);
		}
		%% Первая
		\draw[thick, dashed, blue, marrow] (y3) to[out = 195, in = -15] (x3);
		\draw[thick, dashed, blue, marrow] (y3) to[out = 165, in = 15] (x3);
		%% Вторая
		\draw[thick, dashed, blue, marrow] (yy4) to[out = 195, in = -15] (xx4);
		\draw[thick, dashed, blue, marrow] (yy2) to[out = 165, in = 15] (xx2);
		%% Вторая
		\draw[thick, dashed, blue, marrow] (yyy5) to[out = 195, in = -15] (xxx5);
		\draw[thick, dashed, blue, marrow] (yyy1) to[out = 165, in = 15] (xxx1);
		% Стрелочки
		\draw[thick, ->] (y3) ++(0.5, 0) -- ($(xx3) + (-0.5, 0)$);
		\draw[thick, ->] (yy3) ++(0.5, 0) -- ($(xxx3) + (-0.5, 0)$);
	\end{tikzpicture}
	\caption{}
	\label{fig:Intertwining-relation-usage}
\end{figure}
Теперь нам нужно обработать края и избавиться от вспомогательных рёбер $S_{u-v}$. 

Для этого нам понадобятся ещё два диаграммных тождества, изображённые на рисунке~\ref{fig:Edges}, вывод которых тесно связан со сплетающим соотношением, и доказательства которых также можно посмотреть аппендиксе~\ref{appendix:diagramms}.
\begin{figure}[ht]
	\centering
	\begin{subfigure}[t]{0.7\textwidth}
		\centering
		\begin{tikzpicture}\small
			\def\r{1.25}
			\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
			node[midway, above = 1pt] {$v$} coordinate(w)
				++(30 : \r) coordinate(y0)
				++ (-30 : \r) coordinate(O)	%%Для знака равно
				++ (30 : \r) coordinate(X1);	%%Для правой части
			\draw [thick, marrow] (w) -- ++(180 + 30 : \r)
				node[midway, above = 1pt] {$0$} coordinate(x2);
			\draw [thick, marrow] (w) -- ++ (-30 : \r)
				node[midway, above = 1pt] {$u$} coordinate(y1);
			\draw[fill] (w) circle(2pt);
			%		Вторая часть
			\draw [thick, marrow] (X1) -- ++(-30 : \r)
				node[midway, above = 1pt, blue] {$u$} coordinate(W) ++(30 : \r) coordinate(Y0);
			\draw [thick, marrow] (W) -- ++(180 + 30 : \r)
				node[midway, above = 1pt] {$0$} coordinate(X2);
			\draw [thick, marrow] (W) -- ++ (-30 : \r)
				node[midway, above = 1pt, blue] {$v$} coordinate(Y1);
			\draw[fill] (W) circle(2pt);
			\draw[thick, dashed, blue, marrow] (Y1) to[out = 195, in = -15]
				node[midway, below = 1pt] {$S_{u - v}$} (X2);
			\node at (O) {\Large$=$};
			\foreach \m in {1, 2}{
				\draw
					(x\m) node[left] {$x_\m$}
					(X\m) node[left] {$x_\m$};
			}
			\draw
				(y1) node[right] {$y_1$}
				(Y1) node[right] {$y_1$};
		\end{tikzpicture}
		\caption{Тождество для верхней границы}
		\label{fig:Top-edge}
	\end{subfigure}\\
	\begin{subfigure}[t]{0.7\textwidth}
		\centering
		\begin{tikzpicture}\small
			\def\r{1.25}
			\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
				node[midway, above = 1pt] {$v$} coordinate(w)
				++(30 : \r) coordinate(y0)
				++ (-30 : \r) coordinate(O)	%%Для знака равно
				++ (30 : \r) coordinate(X1);	%%Для правой части
			\draw [thick, marrow] (w) -- ++(180 + 30 : \r)
				node[midway, below right = -1pt] {$0$} coordinate(x2);
			\draw [thick, marrow] (y0) -- ++(180 + 30 : \r)
				node[midway, below right = -1pt] {$0$};
		
			\draw[fill] (w) circle(2pt);
			\draw[thick, dashed, blue, marrow] (y0) to[out = 165, in = 15]
				node[midway, above = 1pt] {$S_{u - v}$} (x1);
			\draw [thick, tail] (x2) -- ++(-30 : 0.75 * \r)
				node[below left] {$v$};
			\draw [thick, tail] (w) -- ++(-30 : 0.75 * \r)
				node[below left] {$u$};
			%		Вторая часть
			\draw [thick, marrow] (X1) -- ++(-30 : \r)
				node[midway, above = 1pt, blue] {$u$} coordinate(W) ++(30 : \r) coordinate(Y0);
			\draw [thick, marrow] (W) -- ++(180 + 30 : \r)
				node[midway, below right = -1pt, blue] {$0$} coordinate(X2);
			\draw [thick, marrow] (Y0) -- ++(180 + 30 : \r)
				node[midway, below right = -1pt, blue] {$0$};
			\draw[fill] (W) circle(2pt);
			\node at (O) {\Large$=$};
			\foreach \m in {1, 2}{
				\draw
					(x\m) node[left] {$x_\m$}
					(X\m) node[left] {$x_\m$}; }
			\draw  
				(y0) node[right] {$y_0$}
				(Y0) node[right] {$y_0$};
			\draw [thick, tail] (X2) -- ++(-30 : 0.75 * \r)
				node[below left, blue] {$u$};
			\draw [thick, tail] (W) -- ++(-30 : 0.75 * \r)
				node[below left, blue] {$v$};
		\end{tikzpicture}
		\caption{Тождество для нижней границы}
		\label{fig:Bottom-edge}
	\end{subfigure}
	\caption{}
	\label{fig:Edges}
\end{figure}

Таким образом, мы аннигилируем вспомогательные рёбра на границах, переставив параметры нужным образом. В результате получаем, что волновая функция инвариантна относительно элементарных транспозиций и следовательно, относительно любой перестановке параметров.
\clearpage
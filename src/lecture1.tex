%% Семинар 1
%% 20.02.20
%%
\section{Сохраняющиеся величины}
\par Рассмотрим цепочку Тоды~--- одномерную систему из $N$ одинаковых частиц с экспоненциальным взаимодействием. Гамильтониан такой системы
\begin{equation}\label{eq:H=definition}
H = \sum_{i=1}^{N} \left( \frac{p_i^2}{2} + e^{x_i - x_{i+1}} \right),
\end{equation}
где $p_i$ и $x_i$~--- импульс и координата $i$-й частицы. Кроме того, нужно зафиксировать граничные условия. Вообще говоря, есть два распространённых выбора
\begin{itemize}[nolistsep,noitemsep]
	\item Открытая цепочка $x_{N+1}\to\infty$
	\item Периодическая цепочка $x_{N+1} = x_{1}$
\end{itemize}

\par При решении механической системы полезным оказывается знание интегралов движения, поэтому сейчас постараемся построить как можно больше таких величин. Во-первых, гамильтониан не имеет явной зависимости от времени, что говорит о сохранении энергии. Во-вторых, система трансляционно инварианта, а значит сохраняется общий импульс
\begin{equation}
P = \sum_{i=1}^N p_i.
\end{equation}
Таким несложным образом мы нашли два интеграла движения, то есть теперь умеем решать задачу о цепочке из двух частиц. Однако для большей цепочки этого не хватит, и надо обзавестись каким-нибудь более универсальным способом построения сохраняющихся величин. К счастью, для цепочки Тоды такой способ существует.

\par Введём для каждой частицы некоторую $2\times2$ матрицу $L_n(u)$, которая зависит от комплексного параметра $u\in\mathbb{C}$,
\begin{equation}\label{eq:L=definition}
L_n (u) = \begin{pmatrix}
u - p_n & e^{-x_n}\\
-e^{x_n}& 0\\
\end{pmatrix}
\end{equation}
и матрицу (монодромии) $T(u)$ как произведение $L$-матриц всех частиц
\begin{equation}\label{eq:T=defintion}
T_N(u) = L_N(u) \ldots L_1(u) = 
\begin{pmatrix}
A(u) & B(u)\\
C(u) & D(u)
\end{pmatrix}
.
\end{equation}
Здесь $A$, $B$, $C$, $D$~--- элементы матрицы $T$. Пока совершенно непонятно, каким образом введенные конструкции связаны с гамильтонианом (\ref{eq:H=definition}). Чтобы это немного прояснить, рассмотрим цепочку из двух частиц
\begin{multline}
	T_2(u) = L_2(u) L_1(u)=
	\begin{pmatrix}
		u - p_2 & e^{-x_2}\\
		-e^{x_2}& 0\\
	\end{pmatrix}
	\begin{pmatrix}
		u - p_n & e^{-x_1}\\
		-e^{x_1}& 0\\
	\end{pmatrix}
	=\\=
	\begin{pmatrix}
		(u - p_1) (u - p_2) - e^{x_1 - x_2} & (u-p_2) e^{-x_1}\\
		-e^{x_2} (u - p_1) & -e^{x_2 - x_1}
	\end{pmatrix}
\end{multline}
и внимательно посмотрим на элемент $A(u)$
\begin{equation}
\label{eq:A(u)N=2}
A(u) = u^2 - u^1 (p_1 + p_2) + u^0\left( p_1 p_2 - e^{x_2 - x_1} \right).
\end{equation}
При некоторой сноровке можно переписать его в виде
\begin{equation}
A(u) = u^2 - u^1 P + u^0\left( \frac{1}{2}P^2 - H_\text{откр.}\right).
\end{equation}
Получилось, что $A(u)$~--- полином по $u$, коэффициенты которого представляют из себя сохраняющиеся величины для случая цепочки с открытыми граничными условиями. Чтобы получить цепочку с периодическими условиями, надо взять не элемент $A$, а след матрицы $T$
\begin{equation}
\tr T(u) = A(u) + D(u) = u^2 - u^1 P + u^0\left( \frac{1}{2}P^2 - H_\text{период.}\right).
\end{equation}
\par Пока что мы разобрались со случаем $N=2$ и увидели там некоторую закономерность. Чтобы убедиться в том, что это не случайное совпадение, посмотрим на случай цепочки из трёх частиц
\begin{multline}
	T_3(u) = 
	\begin{pmatrix}
		u - p_3 & e^{-x_3}\\
		-e^{x_3} & 0
	\end{pmatrix}
	\begin{pmatrix}
		(u - p_1) (u - p_2) - e^{x_1 - x_2} & (u-p_2) e^{-x_1}\\
		-e^{x_2} (u - p_1) & -e^{x_2 - x_1}
	\end{pmatrix}
	=\\=
	\begin{pmatrix}
		\prod\limits_{i=1}^3(u-p_i) - (u-p_3)e^{x_1-x_2} - (u-p_1)e^{x_2-x_3} &
		e^{-x_1} \prod\limits_{i=2}^3(u-p_i)-e^{x_2-x_3-x_1}\\
		-e^{x_3}\prod\limits_{i=1}^2(u-p_i) + e^{x_1 - x_2 + x_3} &
		-(u-p_2)e^{x_3 - x_1}
	\end{pmatrix}.
\end{multline}
Выражение для элемента $A$
\begin{multline}
	A(u) = u^3 - u^2 ( p_1 + p_2 + p_3 ) 
	+ u^1 (p_1p_2 + p_1p_3 + p_2p_3 - e^{x_1 - x_2} - e^{x_2 - x_3}) 
	+\\+
	u^0 ( p_3 e^{x_1 - x_2} + p_2 e^{x_2 - x_3} - p_1 p_2 p_3 ).
\end{multline}
Получилось то же, что и раньше, то есть
\begin{equation}
A(u) = u^3 - u^2 P + u^1 \left(\frac{1}{2}P^2 - H_\text{откр.}\right) + \ldots
\end{equation}
и для периодической
\begin{equation}
A(u)  + D(u) = u^3 - u^2 P + u^1 \left(\frac{1}{2}P^2 - H_\text{период.}\right) + \ldots
\end{equation}
Первые коэффициенты оказались сохраняющимися величинами, посмотрим, не будет ли интегралом движения коэффициент при $u^0$. \textcolor{red}{\textit{Делайте сами}}

Покажем, что старшие коэффициенты полинома всегда будут равны $-P$ и $(\sfrac{1}{2})P^2 - H$.
\begin{theorem}
	Матрица $T_N(u)$ имеет вид
	\begin{multline}
		T_N(u) = u^N \begin{pmatrix}
			1 & 0 \\
			0 & 0 \\
		\end{pmatrix}
		+
		u^{N-1}\begin{pmatrix}
			-P_N & e^{-x_1}\\
			-e^{x_N}& 0\\
		\end{pmatrix}
		+\\+
		u^{N-2}\begin{pmatrix}
			\sum\limits_{1 \le i<k\le N}p_ip_k 
			-\sum\limits_{i=1}^{N-1} e^{x_i-x_{i+1}} &
			-e^{-x_1}(P_N - p_1)\\[12pt]
			e^{x_N}(P_N - p_{N}) & - e^{x_N - x_1}
		\end{pmatrix} + \ldots
	\end{multline}
\end{theorem}
Доказывать это будем по индукции. База уже есть, так как для случая $N=3$ мы посчитали всё явно. Так что остаётся только сделать переход.
\begin{multline}
	T_{N+1}(u) = L_{N+1}(u)T_N(u) = \\=
	(u L^{1} + L^{0} )(u^N T^N + u^{N-1}T^{N-1} + u^{N-2}T^{N-2}+ \ldots)
\end{multline}
Здесь мы временно ввели обозначения для коэффициентов в $T$ и $L$.
\begin{equation}
L^1 = \begin{pmatrix}
1 & 0 \\ 0 & 0\\
\end{pmatrix}
\qquad
L^0 = \begin{pmatrix}
-p_{N+1} & e^{-x_{N+1}}\\
-e^{x_{N+1}} & 0\\
\end{pmatrix}
\end{equation}
Проследим отдельно за всеми коэффициентами. Старший $u^{N+1}$ получается только одним способом
\begin{equation}
L^1 T^N = \begin{pmatrix}
1 & 0 \\
0 & 0 \\
\end{pmatrix}
\end{equation}
Коэффициент при $u^{N}$ можно получить уже двумя способами
\begin{equation}
L^1 T^{N-1} + L^0 T^N =
\begin{pmatrix}
-P_N & e^{-x_1}\\
0 & 0 \\
\end{pmatrix}
+
\begin{pmatrix}
-p_{N+1} & 0 \\
-e^{x_{N+1}} & 0 \\
\end{pmatrix}
=
\begin{pmatrix}
-P_{N+1} & e^{-x_1}\\
-e^{x_{N+1}} & 0 \\
\end{pmatrix}
\end{equation}
И остался только коэффициент при $u^{N-1}$.
\begin{multline}
	L^1 T^{N-2} + L^0 T^{N-1} = 
	\begin{pmatrix}
		\sum\limits_{1 \le i<k\le N}p_ip_k 
		-\sum\limits_{i=1}^{N-1} e^{x_i-x_{i+1}} &
		-e^{-x_1}(P_N - p_1)\\[12pt]
		0 & 0
	\end{pmatrix}
	+\\+
	\begin{pmatrix}
		p_{N+1} P_N  - e^{x_{N} - x_{N+1}} & -p_{N+1} e^{-x_1}\\
		e^{x_{N+1}} P_N & - e^{x_{N+1}-x_1}
	\end{pmatrix}
	=\\=
	\begin{pmatrix}
		\sum\limits_{1 \le i<k\le N+1}p_ip_k 
		-\sum\limits_{i=1}^{N} e^{x_i-x_{i+1}} &
		-e^{-x_1}(P_{N+1} - p_1)\\[12pt]
		e^{x_N}(P_{N+1} - p_{N+1}) & - e^{x_{N+1} - x_1}
	\end{pmatrix}
\end{multline}
Теперь мы видели достаточно, чтобы сформулировать предположение:
\begin{tcolorbox}
	Интегралы движения~--- коэффициенты полинома $A(u)$ в открытой цепочке, и коэффициенты полинома $A(u)+D(u)$ в цепочке с периодическими граничными условиями.
\end{tcolorbox}
То есть утверждается, что $A(u)$ можно представить как сумму
\begin{equation}\label{eq:A(u)=sum of integrals of motion}
A(u) = \sum_{m=0}^{N} (-1)^{m} u^{N-m} H_m,
\end{equation}
где $H_m$~--- интегралы движения (знак выбран из соображений удобства). Можно заметить, что три из них мы уже знаем
\begin{equation}
H_0 =1; \quad H_1 = P_N; \quad H_2 = \frac{1}{2}P^2 - H.
\end{equation}
Заметим, что если $\{A(u),A(v)\} = 0$ при разных параметрах, это значит, что $\{H_m, H_n\} = 0$. Действительно
\begin{equation}
\{A(u),A(v)\} = \sum_{m=0}^{N}\sum_{n=0}^N u^{N-m}v^{N-n} \; \{H_m, H_n\} 
\end{equation}
Всё рассуждение выше верно и на квантовом уровне, так как операторы, которые относятся к разным частицам, коммутируют
\begin{equation*}
p_n=-i\partial_n,\qquad [x_k,p_n]=i\delta_{kn}.
\end{equation*}
\endinput
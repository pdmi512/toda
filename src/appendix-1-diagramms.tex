%% 02.04.20
%%
\section{Сплетающее соотношение}
\label{appendix:diagramms}
В главе про представление Гаусса-Гивенталя мы использовали диаграммную технику для доказательства инвариантности волновых функций относительно перестановок параметров. Для доказательства этого факта мы пользовались сплетающим соотношением и ещё двумя диаграммными тождествами. Сейчас мы их докажем, но для начала стоит напомнить правила Фейнмана,
\begin{equation}
\begin{aligned}
	\begin{tikzpicture}[
		baseline = {(y.base)}]
	\def\r{2}
	\draw [thick, marrow] (0, 0) coordinate(x) -- ++(\r, 0)
		node[midway, above] {$v$} coordinate(y)
		node[midway, below] {\phantom{$v$}};
	\draw[fill]
		(x) node[left] {$x$}
		(y) node[right] {$y$};
	\end{tikzpicture}
	&= \exp \left( i v (x - y) - e^{x - y} \right)\\
	\begin{tikzpicture}[
		baseline = {(y.base)}]
	\def\r{2}
	\draw [thick, tail] (0, 0) coordinate(x) -- ++(0.75 * \r, 0)
		node[pos = 0.66, above] {$v$}
		++ (0.25 * \r, 0) coordinate(y);
	\draw[fill]
		(x) node[left] {$x$}
		(y) node[right] {$\phantom{y}$};
	\end{tikzpicture}
	&= \exp \left( i v x \right)
\end{aligned}
\end{equation}
а также вид сплетающего соотношения
\begin{center}
	\begin{tikzpicture}\small
		\def\r{1.5}
		\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
			node[midway, above = 1pt] {$v$} coordinate(w)
				++(30 : \r) coordinate(y0)
				++ (-30 : \r) coordinate(O)	%%Для знака равно
				++ (30 : \r) coordinate(X1);	%%Для правой части
		\draw [thick, marrow] (w) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$} coordinate(x2);
		\draw [thick, marrow] (y0) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$};
		\draw [thick, marrow] (w) -- ++ (-30 : \r)
			node[midway, above = 1pt] {$u$} coordinate(y1);
		\draw[fill] (w) circle(2pt);
		\draw[thick, dashed, blue, marrow] (y0) to[out = 165, in = 15]
			node[midway, above = 1pt] {$S_{u - v}$} (x1);
%		Вторая часть
		\draw [thick, marrow] (X1) -- ++(-30 : \r)
			node[midway, above = 1pt] {$u$} coordinate(W) ++(30 : \r) coordinate(Y0);
		\draw [thick, marrow] (W) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$} coordinate(X2);
		\draw [thick, marrow] (Y0) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$};
		\draw [thick, marrow] (W) -- ++ (-30 : \r)
			node[midway, above = 1pt] {$v$} coordinate(Y1);
		\draw[fill] (W) circle(2pt);
		\draw[thick, dashed, blue, marrow] (Y1) to[out = 195, in = -15]
			node[midway, above = 1pt] {$S_{u - v}$} (X2);
		\node at (O) {\Large$=$};
		\foreach \n in {0, 1}{
			\pgfmathsetmacro{\m}{int(round(\n + 1))};
			\draw
				(x\m) node[left] {$x_\m$}
				(X\m) node[left] {$x_\m$}
				(y\n) node[right] {$y_\n$}
				(Y\n) node[right] {$y_\n$};
		}
	\end{tikzpicture}
\end{center}
Докажем это тождество и найдём функцию $S_{u - v}$.

Пользуясь правилами Фейнмана, запишем левую часть явно через интегралы
\begin{multline}
	\text{LHS} =
	\int\limits_{- \infty}^\infty \dd{w}
	\exp \big[
		i v (x_1 - w) - e^{x_1 - w} - e^{y_0 - w} \\
		+ i u (w - y_1) - e^{w - y_1} - e^{w - x_2}
	\big]
	\cdot S_{u - v} (y_0, x_1).
\end{multline}
Немного перегруппируем
\begin{multline}
	\text{LHS} = e^{i v x_1 - i u y_1} \int_{-\infty}^\infty \dd{w}
	\exp \big[
	i (u - v) w 
	- e^w \left( e^{- y_1} + e^{- x_2} \right) \\
	- e^{- w} \left( e^{x_1} - e^{y_0}\right)
	\big] \cdot S_{u - v} (y_0, x_1)
\end{multline}
и сделаем замену $t = e^{w}$
\begin{multline}
	\text{LHS} = e^{i v x_1 - i u y_1}
	\int_0^\infty \frac{ \dd{t} }{t}
	t^{i (u - v)}
	\exp \big[
		-t \left( e^{-y_1} + e^{-x_2} \right) \\
		- \frac{1}{t} \left( e^{x_1} + e^{y_0} \right)
	\big]
	\cdot S_{u - v} (y_0, x_1)
	.
\end{multline}
Теперь сделаем ещё одну замену
$ t = s
\left( \frac{
	e^{x_1} + e^{y_0}
}{
	e^{-y_1} + e^{-x_2}
}
\right)^{\frac12}$
\begin{multline}
\label{eq:Intertwining-proof-LHS}
	\text{LHS} = 
	e^{i x_1 v - i y_1 u}
	\left( \frac{
		e^{x_1} + e^{y_0}
	}{
		e^{-y_1} + e^{-x_2}
	}
	\right)^{\frac{i (u - v)}{2}}
	\int_0^\infty \dd{s} s^{i (u - v) - 1}\\
	\cdot \exp \left[
	\sqrt{
		\left( e^{x_1} + e^{y_0} \right)
		\left( e^{-y_1} + e^{-x_2} \right) } \left( s + \frac1s\right)
	 \right] \cdot S_{u - v} (y_0, x_1).
\end{multline}
Внимательно посмотрим на это выражение и заметим, что подынтегральное выражение без  функции $S_{u - v}$ при инверсии меняет параметры $u$, $v$ местами.

Цепочка аналогичных преобразований для правой части сплетающего соотношения приводит к следующему выражению
\begin{multline}
\label{eq:Intertwining-proof-RHS}
	\text{RHS} = 
	e^{i x_1 u - i y_1 v}
	\left( \frac{
		e^{x_1} + e^{y_0}
	}{
		e^{-y_1} + e^{-x_2}
	}
	\right)^{\frac{i (v - u)}{2}}
	\int_0^\infty \dd{s} s^{i (u - v) - 1}\\
	\cdot \exp \left[
	\sqrt{
		\left( e^{x_1} + e^{y_0} \right)
		\left( e^{-y_1} + e^{-x_2} \right) } \left( s + \frac1s\right)
	\right] \cdot S_{u - v} (y_1, x_2).	
\end{multline}
Сравнивая \eqref{eq:Intertwining-proof-LHS} и \eqref{eq:Intertwining-proof-RHS} получаем условие на функцию $S_{u - v}$, при котором сплетающее выражение выполняется
\begin{equation}
	\frac{S_{u-v}(y_0, x_1)}{S_{u-v}(y_1, x_2)} =
	e^{i (u - v) (x_1 + y_1)}
		\left( \frac{
		e^{x_1} + e^{y_0}
	}{
		e^{-y_1} + e^{-x_2}
	}
	\right)^{i (v - u)} =
	\left(
	\frac{
		e^{y_1 - x_2} + 1 }{
		e^{y_0 - x_1} + 1 }
	\right)^{i (u - v)}
\end{equation}
Одним из решений такого уравнения является
\begin{equation}
	S_{u - v} (y, x) = \left( 1 + e^{y - x} \right)^{- i (u - v)}.
\end{equation}
Заметим также, что выполняется условие \eqref{eq:Identity-relation}, то есть
\begin{equation}
	S_{u - v} (y, x) \cdot S_{v - u} (y, x) = 1.
\end{equation}

Теперь докажем ещё два диаграммных тождества, позволяющих убирать вспомогательное ребро $S_{u - v}$ на границах. Первое тождество:
\begin{center}
	\begin{tikzpicture}\small
		\def\r{1.25}
		\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
			node[midway, above = 1pt] {$v$} coordinate(w)
			++(30 : \r) coordinate(y0)
			++ (-30 : \r) coordinate(O)	%%Для знака равно
			++ (30 : \r) coordinate(X1);	%%Для правой части
		\draw [thick, marrow] (w) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$} coordinate(x2);
		\draw [thick, marrow] (w) -- ++ (-30 : \r)
			node[midway, above = 1pt] {$u$} coordinate(y1);
		\draw[fill] (w) circle(2pt);
		%		Вторая часть
		\draw [thick, marrow] (X1) -- ++(-30 : \r)
			node[midway, above = 1pt, blue] {$u$} coordinate(W) ++(30 : \r) coordinate(Y0);
		\draw [thick, marrow] (W) -- ++(180 + 30 : \r)
			node[midway, above = 1pt] {$0$} coordinate(X2);
		\draw [thick, marrow] (W) -- ++ (-30 : \r)
			node[midway, above = 1pt, blue] {$v$} coordinate(Y1);
		\draw[fill] (W) circle(2pt);
		\draw[thick, dashed, blue, marrow] (Y1) to[out = 195, in = -15]
			node[midway, below = 1pt] {$S_{u - v}$} (X2);
		\node at (O) {\Large$=$};
		\foreach \m in {1, 2}{
			\draw
			(x\m) node[left] {$x_\m$}
			(X\m) node[left] {$x_\m$};
		}
		\draw
		(y1) node[right] {$y_1$}
		(Y1) node[right] {$y_1$};
	\end{tikzpicture}
\end{center}
Доказательство получается автоматически из сплетающего соотношения. Рассмотрим пределе $y_0 \rightarrow - \infty$. Тогда левая часть сплетающего соотношения \eqref{eq:Intertwining-proof-LHS}
\begin{multline}
	\text{LHS} =
	\int\limits_{- \infty}^\infty \dd{w}
	\exp \big[
	i v (x_1 - w) - e^{x_1 - w} - e^{y_0 - w} \\
	+ i u (w - y_1) - e^{w - y_1} - e^{w - x_2}
	\big]
	\cdot \left( 1 + e^{y_0 - x_1} \right)^{- i (u - v)}
\end{multline}
в пределе $y_0 \rightarrow - \infty$ равна
\begin{equation}
	\text{LHS} =
	\int\limits_{- \infty}^\infty \dd{w}
	\exp \big[
	i v (x_1 - w) - e^{x_1 - w} + i u (w - y_1) - e^{w - y_1} - e^{w - x_2}
	\big],
\end{equation}
что в точности соответствует левой части доказываемого тождества. Предел правой части сплетающего соотношения ещё проще: мы просто избавляемся от лишней ноги в вершине $y_0$. Таким образом, тождество на верхней границе доказано.

Наконец разберёмся с диаграммным тождеством для нижней границы.
\begin{center}
	\begin{tikzpicture}\small
	\def\r{1.25}
	\draw [thick, marrow] (0, 0) coordinate(x1) -- ++(-30 : \r)
		node[midway, above = 1pt] {$v$} coordinate(w)
		++(30 : \r) coordinate(y0)
		++ (-30 : \r) coordinate(O)	%%Для знака равно
		++ (30 : \r) coordinate(X1);	%%Для правой части
	\draw [thick, marrow] (w) -- ++(180 + 30 : \r)
		node[midway, below right = -1pt] {$0$} coordinate(x2);
	\draw [thick, marrow] (y0) -- ++(180 + 30 : \r)
		node[midway, below right = -1pt] {$0$};

	\draw[fill] (w) circle(2pt);
	\draw[thick, dashed, blue, marrow] (y0) to[out = 165, in = 15]
		node[midway, above = 1pt] {$S_{u - v}$} (x1);
	\draw [thick, tail] (x2) -- ++(-30 : 0.75 * \r)
		node[below left] {$v$};
	\draw [thick, tail] (w) -- ++(-30 : 0.75 * \r)
		node[below left] {$u$};
	%		Вторая часть
	\draw [thick, marrow] (X1) -- ++(-30 : \r)
		node[midway, above = 1pt, blue] {$u$} coordinate(W) ++(30 : \r) coordinate(Y0);
	\draw [thick, marrow] (W) -- ++(180 + 30 : \r)
		node[midway, below right = -1pt, blue] {$0$} coordinate(X2);
	\draw [thick, marrow] (Y0) -- ++(180 + 30 : \r)
		node[midway, below right = -1pt, blue] {$0$};
	\draw[fill] (W) circle(2pt);
	\node at (O) {\Large$=$};
	\foreach \m in {1, 2}{
		\draw
			(x\m) node[left] {$x_\m$}
			(X\m) node[left] {$x_\m$}; }
	\draw  
		(y0) node[right] {$y_0$}
		(Y0) node[right] {$y_0$};
	\draw [thick, tail] (X2) -- ++(-30 : 0.75 * \r)
		node[below left, blue] {$u$};
	\draw [thick, tail] (W) -- ++(-30 : 0.75 * \r)
		node[below left, blue] {$v$};
	\end{tikzpicture}
\end{center}
Для этого честно распишем интегралы. Интеграл в левой части
\begin{multline}
	\text{LHS} =
	\left( 1 + e^{y_0 - x_1} \right)^{- i (u - v)}
	e^{i x_2 v + i x_1 v} \\
	\times
	\int\limits_{- \infty}^\infty \dd{w}
	\exp \big[
	i w (u - v) - e^{x_1 - w} - e^{y_0 - w} - e^{w - x_2}
	\big]
\end{multline}
Делаем точно такие же замены переменных, как и в доказательстве сплетающего соотношения и получаем
\begin{multline}
	\text{LHS} =
	\left( 1 + e^{y_0 - x_1} \right)^{- i (u - v)}
	e^{i x_2 v + i x_1 v}
	\left( \frac{
		e^{y_0} + e^{x_1}
	}{
		e^{-x_2}
	}
	\right)^{\frac{i (u - v)}{2}}
	\\
	\times
	\int\limits_{- \infty}^\infty \dd{w}
	\cdot \exp \left[
	\sqrt{
		\left( e^{x_1}\right)
		\left( e^{-y_1} + e^{-x_2} \right) } \left( s + \frac1s\right)
	\right]
\end{multline}
Правая часть
\begin{multline}
	\text{RHS} = e^{i x_2 u + i x_1 u}
	\left( \frac{
		e^{-x_2}
	}{
		e^{y_0} + e^{x_1}
	}
	\right)^{\frac{i (u - v)}{2}}
	\\
	\times
	\int\limits_{- \infty}^\infty \dd{w}
	\cdot \exp \left[
	\sqrt{
		\left( e^{x_1}\right)
		\left( e^{-y_1} + e^{-x_2} \right) } \left( s + \frac1s\right)
	\right]
\end{multline}
Значит, для чтобы тождество выполнялось, необходимо, чтобы
\begin{equation}
		\left( 1 + e^{y_0 - x_1} \right)^{- i (u - v)}
	e^{i x_2 v + i x_1 v}
	\left( \frac{
		e^{y_0} + e^{x_1}
	}{
		e^{-x_2}
	}
	\right)^{i (u - v)} =  e^{i x_2 u + i x_1 u}.
\end{equation}
Это равенство выполняется. Мы победили.
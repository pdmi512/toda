%% Семинар 7 (темные времена)
%% ?.?.20
%%
\section{От Меллина-Барнса к Гауссу-Гивенталю}
\par В этой части мы покажем, как из одного представления получить другое. Для наглядности, мы рассмотрим только случай $N=3$, поскольку все трудности и идеи возникают уже там.

\par В случае двух частиц переход к представлению Гаусса-Гивенталя требовал только знания определения гамма-функции. Теперь простого превращения гамма-функций в интегралы от экспонент нам не хватит, в случае $N>2$ гамма-функции есть ещё и в знаменателе \eqref{eq:PsiMB}. Чтобы убрать их из знаменателя и написать похожее представление, нам понадобится два ингредиента. 

\par Первый из них~---  $Q$-оператор и то, как он действует на собственные функции
\begin{equation*}
Q_2(\lambda)\Psi(x_1,x_2|\gamma_1,\gamma_2)=\Gamma(i(\lambda-\gamma_1))\Gamma(i(\lambda-\gamma_2))\Psi(x_1,x_2|\gamma_1,\gamma_2).
\end{equation*}
Здесь у нас всё схвачено из предыдущего параграфа.

\par Вторым ингредиентом перехода к другому представлению является следующий интеграл с гамма-функциями
\begin{multline}\label{eq:KarolInt}
%\nonumber
\frac{1}{(2\pi)^22!}\int\dd{\gamma_1}\dd{\gamma_2}\frac{\prod_{j=1}^2\Gamma(i(\lambda_j-\gamma_1))\Gamma(i(\lambda_j-\gamma_2))}{\Gamma(i(\gamma_1-\gamma_2))\Gamma(i(\gamma_2-\gamma_1))}\\[5pt]
%\nonumber
\times\Gamma(i(\gamma_1-\alpha))\Gamma(i(\gamma_2-\alpha))e^{i(\gamma_1+\gamma_2)s}\\[5pt]
=\exp(i(\lambda_1+\lambda_2)s-e^s)\Gamma(i(\lambda_1-\alpha))\Gamma(i(\lambda_2-\alpha)),
\end{multline}
где контур интегрирования находится в области $\Im\alpha<\Im\gamma_{1,2}<\Im\lambda_{1,2}$. 

\par Для доказательства этого соотношения воспользуемся так называемым \textit{первым интегралом Густафсона}
\begin{align*}
\frac{1}{(2\pi)^22!}\int \dd{\gamma_1}\dd{\gamma_2}\frac{\prod_{j=1}^3\Gamma(i(\lambda_j-\gamma_1))\Gamma(i(\lambda_j-\gamma_2))\Gamma(i(\gamma_1-\alpha_j))\Gamma(i(\gamma_2-\alpha_j))}{\Gamma(i(\gamma_1-\gamma_2))\Gamma(i(\gamma_2-\gamma_1))}\\[5pt]
=\frac{\prod_{j,k=1}^3\Gamma(i(\lambda_j-\alpha_k))}{\Gamma(i(\lambda_1+\lambda_2+\lambda_3-\alpha_1-\alpha_2-\alpha_3))},
\end{align*}
интегрирование в котором идет при $\Im\alpha_{1,2,3}<\Im\gamma_{1,2}<\Im\lambda_{1,2,3}$. Наша задача --- превратить его в \eqref{eq:KarolInt}, и мы сделаем это в два шага. Шаг первый: положим 
\begin{equation*}
\lambda_3=Ke^{-t},\quad\alpha_3=-K,\qquad K,t\in\mathbb{R},
\end{equation*}
разделим левую и правую части на $\Gamma^2(iKe^{-t})\Gamma^2(iK)$ и устремим $K\rightarrow+\infty$. В обеих частях у нас возникнут отношения гамма-функций с комплексным аргументом, стремящимся к бесконечности, для которых можно воспользоваться формулой Стирлинга
\begin{gather*}
\frac{\Gamma(i(Ke^{-t}-\beta))}{\Gamma(iKe^{-t})}\sim\bigl(iKe^{-t}\bigl)^{-i\beta}e^{i\beta},\quad\frac{\Gamma(i(\beta+K))}{\Gamma(iK)}\sim\bigl(iK\bigl)^{i\beta}e^{-i\beta},\\
\frac{\Gamma(i(Ke^{-t}+K))}{\Gamma(i(Ke^{-t}+K+\beta))}\sim\bigl(iK(1+e^{-t})\bigl)^{-i\beta}e^{i\beta},
\end{gather*} 
где в роли $\beta$ будут выступать $\lambda_j,\gamma_j,\alpha_j$ и $\lambda_1+\lambda_2-\alpha_1-\alpha_2$. Применяя эти соотношения к интегралу Густафсона, в пределе получаем формулу
\begin{align*}
\frac{1}{(2\pi)^22!}\int \dd{\gamma_1}\dd{\gamma_2}\frac{\prod_{j=1}^2\Gamma(i(\lambda_j-\gamma_1))\Gamma(i(\lambda_j-\gamma_2))\Gamma(i(\gamma_1-\alpha_j))\Gamma(i(\gamma_2-\alpha_j))}{\Gamma(i(\gamma_1-\gamma_2))\Gamma(i(\gamma_2-\gamma_1))}\\
\times e^{i(\gamma_1+\gamma_2)t}=\bigl(1+e^t\bigl)^{i(\alpha_1+\alpha_2-\lambda_1-\lambda_2)t}e^{i(\lambda_1+\lambda_2)t}\prod_{j,k=1}^2\Gamma(i(\lambda_j-\alpha_k)).
\end{align*}

\par Следующий шаг --- ещё одна редукция, положим 
\begin{equation*}
\alpha_2=-K,\quad t=s-\ln(iK),\qquad K,s\in\mathbb{R},
\end{equation*}
разделим обе части на $\Gamma^2(iK)$ и снова устремим $K\rightarrow+\infty$. Выпишем множители, содержащие $K$, под интегралом слева
\begin{equation*}
\frac{\Gamma(i(\gamma_1+K))\Gamma(i(\gamma_2+K))}{\Gamma^2(iK)}e^{-i(\gamma_1+\gamma_2)\ln(iK)}\longrightarrow 1,
\end{equation*}
и также справа
\begin{multline*}
\frac{\Gamma(i(\lambda_1+K))\Gamma(i(\lambda_2+K))}{\Gamma^2(iK)}e^{-i(\lambda_1+\lambda_2)\ln(iK)}\bigl(1+e^s/iK\bigl)^{i(\alpha_1-\lambda_1-\lambda_2)-iK}\\
\longrightarrow \exp(-e^s).
\end{multline*}
Получившееся в этом втором пределе соотношение совпадает с \eqref{eq:KarolInt} после замены $\alpha_1\rightarrow\alpha$. 

\par Теперь все ингредиенты собраны, и мы можем осуществить переход от Меллина-Барнса к Гауссу-Гивенталю. Первая мысль: нужно увидеть интеграл \eqref{eq:KarolInt} в выражении для собственной функции 
\begin{multline*}
\Psi(\pmb{x}|\pmb{\lambda})=\int \dd{\gamma_1}\dd{\gamma_2}\frac{\prod_{j=1}^3\Gamma(i(\lambda_j-\gamma_1))\Gamma(i(\lambda_j-\gamma_2))}{\Gamma(i(\gamma_1-\gamma_2))\Gamma(i(\gamma_2-\gamma_1))}\\[4pt]
\times e^{i(\lambda_1+\lambda_2+\lambda_3-\gamma_1-\gamma_2)x_3}\Psi(x_1,x_2|\gamma_1,\gamma_2)
\end{multline*}
Для этого под интегралом воспользуемся $Q$-оператором, чтобы убрать две гамма-функции, и запишем двухчастичную функцию в представлении Меллина-Барнса
\begin{align*}
\Psi(\pmb{x}|\pmb{\lambda})=Q_2(\lambda_3)e^{i(\lambda_1+\lambda_2+\lambda_3)x_3}\int \dd{\gamma_1}\dd{\gamma_2}\dd{\alpha}\frac{\prod_{j=1}^2\Gamma(i(\lambda_j-\gamma_1))\Gamma(i(\lambda_j-\gamma_2))}{\Gamma(i(\gamma_1-\gamma_2))\Gamma(i(\gamma_2-\gamma_1))}\\[3pt]
\times\Gamma(i(\gamma_1-\alpha))\Gamma(i(\gamma_2-\alpha))e^{i(\gamma_1+\gamma_2)(x_2-x_3)}e^{i\alpha(x_1-x_2)}.
\end{align*}
Получился нужный интеграл с $s=x_2-x_3$. Возьмём интегралы по $\gamma_{1,2}$, тогда
\begin{align*}
\Psi(\pmb{x}|\pmb{\lambda})=Q_2(\lambda_3)e^{i(\lambda_1+\lambda_2+\lambda_3)x_3}\int \dd{\alpha}\Gamma(i(\lambda_1-\alpha))\Gamma(i(\lambda_2-\alpha))\\
\times\exp(i(\lambda_1+\lambda_2)(x_2-x_3)-e^{x_2-x_3})e^{i\alpha(x_1-x_2)}.
\end{align*}
Интеграл по $\alpha$ снова дает нам двухчастичную функцию, а значит, вспоминая, как выглядит $Q$-оператор при $N=2$ \eqref{eq:QN=2},
\begin{align*}
\Psi(\pmb{x}|\pmb{\lambda})&=Q_2(\lambda_3)\exp(i\lambda_3x_3-e^{x_2-x_3})\Psi(x_1,x_2|\lambda_1,\lambda_2)\\
&=\int\dd{y_1}\dd{y_2}\exp\bigl(i\lambda_3(x_1+x_2+x_3-y_1-y_2)\\
&\qquad\qquad\quad\,\,-e^{y_2-x_3}-e^{x_2-y_2}-e^{y_1-x_2}-e^{x_1-y_1}\bigl)\Psi(y_1,y_2|\lambda_1,\lambda_2),
\end{align*}
мы получаем собственную функцию в представлении Гаусса-Гивенталя.

\par Итого, для трех частиц показали эквивалентность двух представлений, что символически (с точностью до числовых коэффициентов, которые мы по дороге забывали) можно записать так
\begin{align*}
\Psi(x_1,x_2,x_3|\lambda_1,\lambda_2,\lambda_3)&=\int \dd^2\pmb{\gamma}\,\mathcal{M}(\lambda_1,\lambda_2,\lambda_3|\gamma_1,\gamma_2,x_3)\Psi(x_1,x_2|\gamma_1,\gamma_2)\\
&=\int \dd^2\pmb{y}\,\mathcal{G}(x_1,x_2,x_3|y_1,y_2,\lambda_3)\Psi(y_1,y_2|\lambda_1,\lambda_2).
\end{align*}

\par На самом деле, даже не слишком внимательный читатель заметит, что мощное тождество \eqref{eq:KarolInt}, которое нас здесь спасло, мы не доказали честно, но вывели из более известного интеграла Густафсона. Открытая проблема: как вывести его независимо (в том числе при произвольном $N$) и относительно просто (из какого-нибудь локального соотношения, например). Или же вывести интеграл Густафсона как-то по-простому, а не как это сделано в оригинальной статье.

\par Кроме того, в ходе вычисления мы несколько раз переставляли пределы и интегралы местами. Аккуратный вывод со всеми нужными оговорками (и при произвольном $N$) можно посмотреть у Кароля Козловски.
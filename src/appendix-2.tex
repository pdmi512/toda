\section{Тождество с $R$-оператором}
\label{appendix:R}
В параграфе о представлении Гаусса-Гивенталя, утверждается, что $R$-оператор, заданный как интегральный оператор на функциях двух переменных,
\begin{multline}
\label{a:R-operator}
	\bigl(
		R_{12} (v) f
	\bigl) (x_1, x_2)\\
	=\int \dd{y_1} \dd{y_2}
	\exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	\delta (y_2 - x_1) f (y_1, y_2)
\end{multline}
переставляет местами $L$-операторы для цепочки Тоды и DST-цепочки
\begin{equation}
\label{a:RLM}
	R_{12} (v) L_1 (u) M_2 (u - v) =
	M_2 (u - v) L_1 (u) R_{12} (v)
	.
\end{equation}
Напоминаю, что $L$-операторы для цепочки Тоды и DST-цепочки имеют следующий вид:
\begin{equation}
	L_n(u) = 
	\begin{pmatrix}
	u - p_n & e^{-x_n}\\
	- e^{x_n} & 0\\
	\end{pmatrix}
	,
	\qquad
	M_n (u) =
	\begin{pmatrix}
		u - p_n & e^{-x_n}\\
		-i e^{x_n} p_n & i\\
	\end{pmatrix}.
\end{equation}
Докажем утверждение \eqref{a:RLM} прямой проверкой. Для дальнейшего удобства сначала запишем матрицы $L_1 (u) M_2 (u - v)$ и $M_2 (u - v) L_1 (u)$ в явном виде
\begin{multline}
	L_1 (u) M_2(u - v) =\\
	\begin{pmatrix}
		(u + i \partial_1) (u - v + i \partial_2) - e^{x_2 - x_1} \partial_2 &
		(u + i \partial_1) e^{-x_2} + i e^{-x_1}\\
		-(u - v + i \partial_2) e^{x_1} &
		- e^{x_1 - x_2}
	\end{pmatrix}
\end{multline}
\begin{multline}
	M_2(u - v) L_1 (u) =\\
	\begin{pmatrix}
		(u + i \partial_1) (u - v + i \partial_2) - e^{x_1 - x_2} &
		(u - v + i \partial_2) e^{-x_1}\\
		-(u + i \partial_1) e^{x_2} \partial_2 - i e^{x_1} &
		- e^{x_2 - x_1} \partial_2
	\end{pmatrix}
\end{multline}
Начнём проверку с самого простого элемента~--- $(2,2)$. Запишем ядра операторов в левой и правой части уравнения \eqref{a:RLM}. Ядро оператора в левой части
\begin{equation*}
	\text{LHS}_{22} \dot=
	\exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	\delta (y_2 - x_1)
	\left(-e^{y_1 - y_2} \right)
\end{equation*}
Здесь и далее символ <<$\dot =$>> обозначает равенство ядер.
Ядро оператора в правой части
\begin{multline*}
	\text{RHS}_{22} \dot= -e^{x_2 - x_1} \partial_{x_2}
	\exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	\delta (y_2 - x_1)\\
	=-e^{y_1 - x_1}
	\exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	\delta (y_2 - x_1)
\end{multline*}
В ядре оператора с $\delta$-функцией от переменных $x_1$ и $y_2$ эти переменные можно безболезненно заменять друг на друга, поэтому получаем равенство тождественное равенство ядер операторов для элементов $(2, 2)$ в правой и левой части.

Проделаем аналогичные действия для элементов $(2, 1)$. Ядро оператора в левой части
\begin{multline*}
	\text{LHS}_{21}\\
	\dot = 
	\exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	\delta (y_2 - x_1)
	(- u + v - i \partial_{y_2}) e^{y_1}.
\end{multline*}
Проинтегрируем по частям, то есть перебросим производную $\partial_{y_2}$ с функции двух переменных на остальное подынтегральное выражение. Тогда ядро интегрального оператора преобразуется к следующему виду:
\begin{multline*}
	\text{LHS}_{21}
	\dot =
	\exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)\\
	\times \left[
	(v - u) \delta (y_2 - x_1) + i\delta'(y_2 - x_1)
	\strut\right]
	e^{y_1}
\end{multline*}
Ядро оператора в правой части
\begin{multline*}
	\text{RHS}_{21} 
	\dot =
	\left( -(u + i \partial_{x_1}) e^{x_2} \partial_{x_2} - i e^{x_1} \strut \right) \\
	\times \exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	\delta (y_2 - x_1)
\end{multline*}
Распишем преобразования по шагам, чтобы не запутаться. Сначала продифференцируем по $x_2$
\begin{multline*}
	\text{RHS}_{21}
	\dot =
	\left( -(u + i \partial_{x_1}) e^{y_1} - i e^{x_1} \strut \right) \\
	\times \exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	\delta (y_2 - x_1),
\end{multline*}
затем возьмём производную по $x_1$. Обратим внимание, что производная также действует на $\delta$-функцию
\begin{multline*}
	\text{RHS}_{21}
	\dot =
	\left[ \left( -u + v + i e^{x_1 - y_1}) e^{y_1} - i e^{x_1} \strut \right) \delta (y_2 - x_1) + i \delta' (y_2 - x_1) \right] \\
	\times \exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
\end{multline*}
Аккуратно преобразуем и получим выражение для ядра
\begin{multline*}
	\text{RHS}_{21}
	\dot =
	\left[ ( v - u ) \delta (y_2 - x_1) + i \delta' (y_2 - x_1) \right] e^{y_1} \\
	\times \exp \left(
	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
	\right)
	,
\end{multline*}
что в точности совпадает с левой частью.

Для элементов $(1, 2)$ и $(1, 1)$ доказательство аналогично.
\TODO{Написать?}
%Рассмотрим элемент $(1, 2)$. Ядро оператора в левой части
%\begin{multline*}
%	\text{LHS}_{12} \\
%	= \exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%	\delta (y_2 - x_1)
%	\left[ (u + i \partial_{y_1}) e^{-y_2} + i e^{-y_1} \right].
%\end{multline*}
%Интегрируем по частям
%\begin{multline*}
%	\text{LHS}_{12}
%	= \exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%	\delta (y_2 - x_1) \\
%	\times
%	\left[ (u - v - i e^{x_1 - y_1} + i e^{y_1 - x_2}) e^{-y_2} + i e^{-y_1} \right]
%\end{multline*}
%Итого, ядро оператора в левой части равно
%\begin{multline*}
%	\text{LHS}_{12}
%	= \exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%	\delta (y_2 - x_1) \\
%	\times
%	\left[ (u - v) e^{-y_2} + i e^{y_1 - x_2 - y_2}) \right]
%\end{multline*}
%Ядро оператора в правой части
%\begin{multline*}
%	\text{RHS}_{12} \\
%	= (u - v + i \partial_{x_2}) e^{-x_1}
%	\exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%	\delta (y_2 - x_1)
%\end{multline*}
%Действуем производной и преобразуем
%\begin{multline*}
%	\text{RHS}_{12}\\
%	= (u - v + i e^{y_1 - x_2}) e^{-y_2}
%	\exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%	\delta (y_2 - x_1)
%\end{multline*}
%Получаем, что ядра операторов в левой и правой частях равны.
%
%Наконец, проверим элемент $(1, 1)$. Ядро оператора в левой части
%\begin{multline*}
%	\text{LHS}_{11}
%	=
%	\exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%	\delta (y_2 - x_1) \\
%	\times
%	\left[ (u + i \partial_{y_1}) (u - v + i \partial_{y_2}) - e^{y_2 - y_1} \partial_{y_2} \right]
%\end{multline*}
%Интегрируем по частям
%\begin{multline*}
%	\text{LHS}_{11}
%	=
%	\exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right) \\
%	\times
%	\left[ (u - v - i e^{x_1 - y_1} + i e^{y_1 - x_2}) (u - v) - e^{y_2 - y_1} \right] \delta (y_2 - x_1) \\
%	+
%	\exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right) \\
%	\times
%	\left[ -i (u - v - i e^{x_1 - y_1} + i e^{y_1 - x_2}) + e^{y_2 - y_1} \right] \delta' (y_2 - x_1)
%\end{multline*}
%Ядро оператора в правой части
%\begin{multline*}
%	\text{RHS}_{11}
%	=
%	\left[
%	(u + i \partial_{x_1}) (u - v + i \partial_{x_2}) - e^{x_1 - x_2}
%	\right] \\
%	\times
%	\exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%	\delta (y_2 - x_1)
%\end{multline*}
%Дифференцируем по $x_1$ и $x_2$
%\begin{multline*}
%	\text{RHS}_{11}
%	=
%	\left[
%	(u - v - i e^{x_1 - y_1}) (u - v + i e^{y_1 - x_2}) - e^{x_1 - x_2}
%	\right] \\
%	\times \exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right) \delta(y_2 - x_1) \\
%	-
%	i (u - v + i e^{y_1 - x_2}) \delta'(y_2 - x_1)
%	\times
%	\exp \left(
%	i v (x_1 - y_1) - e^{x_1 - y_1} - e^{y_1 - x_2}
%	\right)
%\end{multline*}
%Сравниваем с левой частью и получаем, равенство. Таким образом, доказали утверждение \eqref{a:RLM} явно.